# escape=`

FROM abrarov/msvc-2019:2.10.0

# Install scoop
RUN powershell.exe -ExecutionPolicy RemoteSigned `
  iex (new-object net.webclient).downloadstring('https://get.scoop.sh');

# Install python, git via scoop to use later on
RUN scoop install python git

# Install aqtinstall and then install version of Qt for build
RUN pip install aqtinstall
RUN aqt install 5.15.2 windows desktop win32_msvc2019
RUN aqt install 5.15.2 windows desktop win64_msvc2019_64

# Use developer command prompt and start PowerShell if no other command specified
ENTRYPOINT ["C:\\Program Files (x86)\\Microsoft Visual Studio\\2019\\Community\\Common7\\Tools\\VsDevCmd.bat", "&&", "powershell.exe", "-NoLogo", "-ExecutionPolicy", "Bypass"]